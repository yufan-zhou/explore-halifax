# Explore Halifax

- Add your username to the Excel sheet that can be found Files -> Assignments -> Gitlab_usernames.xlsx
- Clone the repository
- Add your contributions by changing guide.md file (and optionally adding images in the images folder)
- Commit your changes
- Push to the Gitlab repository before the deadline
